# README #

Good, this is a project for sending and receiving messages between users through REST services.

Tech used:
----------

### Language
- Java 7

### Build and dependency management
- Gson (http://www.java2s.com/Code/Jar/g/Downloadgson222jar.htm)

### Application Server
-JBoss EAP 6.0

### Database
The project needs a database by eg: exercise-message.

With the following tables

	create table usuario(
		"id" SERIAL primary key,
		"nombre" varchar(50) not null,
		"estado" SMALLINT not null
	);

	create table login(
		"id" SERIAL primary key,
		"usuario" varchar(50) not null,
		"password" varchar(50) not null,
		"estado" SMALLINT not null
	);

	create table message(
		"id" SERIAL primary key,
		"mensaje" varchar(100) not null,
		"id_from" BIGINT REFERENCES usuario(id) not null,
		"id_to" bigint REFERENCES usuario(id) not null,
		"fecha_envio" date not null,
		"fecha_visto" date
	);

# Description #


The project manages security by validating that the services are consumed by users stored in "Login".

Credentials on all services are sent encrypted by the client's IP.

The services that are proposed are the following:

### v 0.1.02 - 2017/02/10
    Services
		* User
			AddUser - POST: /Rest/User/AddUser
			UpdateUser - POST: /Rest/User/UpdateUser
			DeleteUser - POST: /Rest/User/DeleteUser
		*Message
			Messages/{name} - GET: /Rest/Message/Messages/{name}
			Message - POST: /Rest/Message/SendMessage