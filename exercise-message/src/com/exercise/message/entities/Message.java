package com.exercise.message.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the message database table.
 * 
 */
@Entity
@NamedQuery(name = "Message.findAll", query = "SELECT m FROM Message m")
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MESSAGE_ID_GENERATOR", sequenceName = "MESSAGE_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MESSAGE_ID_GENERATOR")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_envio")
	private Date fechaEnvio;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_visto")
	private Date fechaVisto;

	private String mensaje;

	// bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "id_from")
	private Usuario usuario1;

	// bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "id_to")
	private Usuario usuario2;

	public Message() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFechaEnvio() {
		return this.fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Date getFechaVisto() {
		return this.fechaVisto;
	}

	public void setFechaVisto(Date fechaVisto) {
		this.fechaVisto = fechaVisto;
	}

	public String getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Usuario getUsuario1() {
		return this.usuario1;
	}

	public void setUsuario1(Usuario usuario1) {
		this.usuario1 = usuario1;
	}

	public Usuario getUsuario2() {
		return this.usuario2;
	}

	public void setUsuario2(Usuario usuario2) {
		this.usuario2 = usuario2;
	}

}