package com.exercise.message.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.exercise.message.entities.Message;
import com.exercise.message.entities.input.SendMensajeInput;
import com.exercise.message.entities.output.MensajeOutput;
import com.exercise.message.entities.output.Resultado;
import com.exercise.message.negocio.MessageBL;
import com.exercise.message.negocio.UsuarioBL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/Message")
public class MessageServices extends Servicios {
	@Inject
	MessageBL messageBL;

	@Inject
	UsuarioBL usuarioBL;

	@GET
	@Path("/HolaMundo")
	public String HolaMundo() {
		return "Hola Mensajes";
	}

	@GET
	@Path("/Messages/{nombre}/{credenciales}")
	public String ListarMensajes(@Context HttpServletRequest requestContext, @PathParam("nombre") String nombre,
			@PathParam("credenciales") String credenciales) {
		try {

			AutorizadoByCredenciales(requestContext, credenciales);

			List<Message> listMensajes = messageBL.LeerMensajes(nombre);

			List<MensajeOutput> listMensajeOutput = new ArrayList<MensajeOutput>();

			if (listMensajes != null) {
				if (listMensajes.size() > 0) {
					for (Message message : listMensajes) {
						MensajeOutput mensajeOutput = new MensajeOutput();

						mensajeOutput.setFechaEnvio(message.getFechaEnvio());
						mensajeOutput.setMensaje(message.getMensaje());
						mensajeOutput.setUsuarioFrom(message.getUsuario1().getNombre());
						mensajeOutput.setUsuarioTo(message.getUsuario2().getNombre());

						listMensajeOutput.add(mensajeOutput);
					}
				}
			}
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.setDateFormat("yyyy-MM-dd");
			Gson gson = gsonBuilder.create();

			return Resultado(0, Resultado.Mensaje_OK, gson.toJson(listMensajeOutput));
		} catch (Exception ex) {
			ex.printStackTrace();
			return Resultado(1, ex.getMessage(), null);
		}
	}

	@POST
	@Path("/SendMessage")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String EnviarMensaje(@Context HttpServletRequest requestContext, String input) {
		try {
			AutorizadoByInput(requestContext, input);

			SendMensajeInput mensajeInput = new Gson().fromJson(eInput.getObject(), SendMensajeInput.class);

			Message message = new Message();

			message.setMensaje(mensajeInput.getMensaje());
			message.setFechaEnvio(new Date());
			message.setFechaVisto(null);

			message.setUsuario1(usuarioBL.BuscarUsuarioByNombre(mensajeInput.getNombreFrom()));
			message.setUsuario2(usuarioBL.BuscarUsuarioByNombre(mensajeInput.getNombreTo()));

			messageBL.Save(message, usuarioBL.BuscarUsuarioByNombre(mensajeInput.getNombreFrom()),
					usuarioBL.BuscarUsuarioByNombre(mensajeInput.getNombreTo()));

			return Resultado(0, Resultado.Mensaje_OK, null);

		} catch (Exception ex) {
			ex.printStackTrace();
			return Resultado(1, ex.getMessage(), null);
		}
	}

}
