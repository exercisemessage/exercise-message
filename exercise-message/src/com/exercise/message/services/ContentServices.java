package com.exercise.message.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/Servicios")
public class ContentServices {
	@GET
	@Path("/HolaMundo")
	public String HolaMundo(){
		return "Hola Mundo";
	}
}
