package com.exercise.message.services;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.exercise.message.entities.Login;
import com.exercise.message.entities.input.Credenciales;
import com.exercise.message.entities.input.Input;
import com.exercise.message.entities.output.Resultado;
import com.exercise.message.negocio.LoginBL;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class Servicios {
	@Inject
	LoginBL login;

	protected Input eInput;

	protected String Resultado(int codigo, String mensaje, String objetoJson) {
		Resultado resultado = new Resultado(codigo, mensaje, objetoJson);

		return new Gson().toJson(resultado);
	}

	private boolean Autorizado(String credencial, String yourIP) throws Exception {
		boolean sw = false;
		List<Login> listLogin = login.get();

		for (Login login : listLogin) {
			Credenciales cred = new Credenciales(login.getUsuario(), login.getPassword());

			System.out.println(yourIP);
			System.out.println(login.getUsuario());
			System.out.println(login.getPassword());

			String credEncriptado = cred.Encriptar(yourIP);

			System.out.println("yoyo: " + credEncriptado);
			System.out.println("Pedro: " + credencial);
			if (credencial.equals(credEncriptado)) {
				sw = true;
				break;
			}
		}

		return sw;
	}

	protected void AutorizadoByInput(HttpServletRequest requestContext, String input)
			throws JsonSyntaxException, Exception {
		String yourIP = requestContext.getRemoteAddr();

		eInput = new Input(input, yourIP);

		if (!Autorizado(eInput.getCredenciales(), yourIP)) {
			throw new Exception("Error usuario no autorizado.");
		}
	}

	protected void AutorizadoByCredenciales(HttpServletRequest requestContext, String credenciales)
			throws com.google.gson.JsonSyntaxException, Exception {
		String yourIP = requestContext.getRemoteAddr();

		if (!Autorizado(credenciales, yourIP)) {
			throw new Exception("Error usuario no autorizado.");
		}
	}
}
