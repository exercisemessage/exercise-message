package com.exercise.message.services;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.exercise.message.entities.Estado;
import com.exercise.message.entities.Usuario;
import com.exercise.message.entities.input.DeleteUsuarioInput;
import com.exercise.message.entities.input.NewUsuarioInput;
import com.exercise.message.entities.input.UpdateUsuarioInput;
import com.exercise.message.entities.output.Resultado;
import com.exercise.message.negocio.UsuarioBL;
import com.google.gson.Gson;

@Path("/User")
public class UserServices extends Servicios {
	@Inject
	UsuarioBL usuarioBL;

	@GET
	@Path("/HolaMundo")
	public String HolaMundo() {
		return "Hola Usuario";
	}

	@POST
	@Path("/AddUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String Save(@Context HttpServletRequest requestContext, String input) {
		try {
			AutorizadoByInput(requestContext, input);

			NewUsuarioInput usuarioInput = new Gson().fromJson(eInput.getObject(), NewUsuarioInput.class);

			Usuario usuario = new Usuario();

			usuario.setNombre(usuarioInput.getNombre());
			usuario.setEstado(Estado.Habilitado);

			this.usuarioBL.Save(usuario);

			return Resultado(0, Resultado.Mensaje_OK, null);
		} catch (Exception ex) {
			ex.printStackTrace();
			return Resultado(1, ex.getMessage(), null);
		}
	}

	@POST
	@Path("/DeleteUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String EliminarUsuario(@Context HttpServletRequest requestContext, String input) {
		try {
			AutorizadoByInput(requestContext, input);

			DeleteUsuarioInput deleteUsuarioInput = new Gson().fromJson(eInput.getObject(), DeleteUsuarioInput.class);

			Usuario usuario = usuarioBL.BuscarUsuarioByNombre(deleteUsuarioInput.getNombre());

			usuarioBL.Delete(usuario);

			return Resultado(0, Resultado.Mensaje_OK, null);
		} catch (Exception ex) {
			ex.printStackTrace();
			return Resultado(1, ex.getMessage(), null);
		}

	}

	@POST
	@Path("/UpdateUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String ActualizarUsuario(@Context HttpServletRequest requestContext, String input) {
		try {
			AutorizadoByInput(requestContext, input);

			UpdateUsuarioInput updateUsuarioInput = new Gson().fromJson(eInput.getObject(), UpdateUsuarioInput.class);

			Usuario usuario = usuarioBL.BuscarUsuarioByNombre(updateUsuarioInput.getOldNombre());

			usuario.setNombre(updateUsuarioInput.getNewNombre());

			usuarioBL.Update(usuario);

			return Resultado(0, Resultado.Mensaje_OK, null);
		} catch (Exception ex) {
			ex.printStackTrace();
			return Resultado(1, ex.getMessage(), null);
		}
	}
}
