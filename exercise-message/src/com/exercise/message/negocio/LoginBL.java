package com.exercise.message.negocio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.exercise.message.dao.LoginDAO;
import com.exercise.message.entities.Login;

@Named
public class LoginBL {
	@Inject
	LoginDAO loginDAO;
	
	
	public List<Login> get()throws Exception{
		return loginDAO.Listar();
	}
}
