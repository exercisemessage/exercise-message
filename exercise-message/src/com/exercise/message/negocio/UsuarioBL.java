package com.exercise.message.negocio;

import javax.inject.Inject;
import javax.inject.Named;

import com.exercise.message.dao.UsuarioDAO;
import com.exercise.message.entities.Estado;
import com.exercise.message.entities.Usuario;

@Named
public class UsuarioBL {
	@Inject
	UsuarioDAO usuarioDAO;

	public void Save(Usuario usuario) throws Exception {
		Usuario aux = BuscarUsuarioByNombre(usuario.getNombre());
		if (aux != null) {
			throw new Exception("Error: El usuario ya existe en la Coleccion de Datos.");
		}
		usuarioDAO.Save(usuario);
	}

	public void Update(Usuario usuario) throws Exception {
		usuarioDAO.Update(usuario);

	}

	public Usuario BuscarUsuarioByNombre(String nombre) throws Exception {
		return usuarioDAO.BuscarUsuarioByNombre(nombre);
	}

	public void Delete(Usuario usuario) throws Exception {
		usuario.setEstado(Estado.Eliminado);

		usuarioDAO.Update(usuario);
	}
}
