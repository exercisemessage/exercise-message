package com.exercise.message.negocio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.exercise.message.dao.MessageDAO;
import com.exercise.message.entities.Message;
import com.exercise.message.entities.Usuario;

@Named
public class MessageBL {
	@Inject
	MessageDAO messageDAO;
	
	public void Save(Message u,Usuario us1, Usuario us2) throws Exception{
		messageDAO.Save(u,us1,us2);
	}
	
	public List<Message> LeerMensajes(String nombreUsuario) throws Exception {

		return messageDAO.LeerMensajes(nombreUsuario);
	}
}
