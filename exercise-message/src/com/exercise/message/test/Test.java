package com.exercise.message.test;

import java.lang.reflect.Type;
import java.util.List;

import javax.ws.rs.core.MediaType;

import com.exercise.message.entities.Usuario;
import com.exercise.message.entities.input.Credenciales;
import com.exercise.message.entities.input.Input;
import com.exercise.message.entities.output.MensajeOutput;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Test {
	public static void main(String[] args) {
		
		System.out.println(MediaType.APPLICATION_JSON);
		String encryptionKey = "192.168.0.106";
		
		try {
			Usuario usuario = new Usuario();
			usuario.setNombre("Adilson");
			usuario.setEstado(1);
			
			Credenciales cr = new Credenciales("admin", "admin");
			
			Input input = new Input();
			
			input.setCredenciales(cr.Encriptar(encryptionKey));
			input.setObject(new Gson().toJson(usuario));
			
			System.out.println(input.getCredenciales());
			System.out.println(input.getObject());
			
			String sGson = new Gson().toJson(input);
			
			System.out.println(sGson);
			
			Input aux = new Gson().fromJson(sGson, Input.class);
			
			System.out.println(aux.getCredenciales());
			System.out.println(aux.getObject());
			
			System.out.println("[{\"Mensaje\"\"sucia\",\"UsuarioFrom\"\"thalia\",\"UsuarioTo\"\"shakira\",\"FechaEnvio\"\"feb 10, 2017\"}]");
			String cadena = "[{\"Mensaje\":\"sucia\",\"UsuarioFrom\":\"thalia\",\"UsuarioTo\":\"shakira\",\"FechaEnvio\":\"feb 10, 2017\"}]";
			Type listType = new TypeToken<List<MensajeOutput>>() {}.getType();
			List<MensajeOutput> lista = new Gson().fromJson(cadena, listType);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
