package com.exercise.message.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.exercise.message.entities.Login;

@Named
public class LoginDAO {

	@PersistenceContext(unitName = "exercise-message")
	private EntityManager entity;
	@Resource
	private UserTransaction userTransaction;

	@SuppressWarnings("unchecked")
	public List<Login> Listar() throws Exception {
		return entity.createQuery("SELECT us FROM Login us where us.estado=1 Order by us.id").getResultList();
	}
}
