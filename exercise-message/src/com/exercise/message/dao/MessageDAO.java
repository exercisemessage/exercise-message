package com.exercise.message.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import com.exercise.message.entities.Message;
import com.exercise.message.entities.Usuario;

@Named
public class MessageDAO {

	@PersistenceContext(unitName = "exercise-message")
	private EntityManager entity;
	@Resource
	private UserTransaction userTransaction;

	public void Save(Message message,Usuario us1, Usuario us2) throws Exception {
		userTransaction.begin();
		entity.persist(message);
//		message.setUsuario1(us1);
//		message.setUsuario2(us2);
//		entity.merge(message);
		userTransaction.commit();
		

	}

	@SuppressWarnings("unchecked")
	public List<Message> LeerMensajes(String nombreUsuario) throws Exception {

		String consulta = "SELECT us FROM Message us WHERE  us.fechaVisto=NULL and us.usuario2.nombre=:nombreUsuario";
		Query query = entity.createQuery(consulta).setParameter("nombreUsuario", nombreUsuario);

		List<Message> listMessage = query.getResultList();

		if (listMessage != null) {
			if (listMessage.size() > 0) {
				for (Message message : listMessage) {
					message.setFechaVisto(new Date());

					Update(message);
				}
			}
		}

		return listMessage;
	}

	public void Update(Message message) throws Exception {
		userTransaction.begin();
		entity.merge(message);
		userTransaction.commit();
	}

}
