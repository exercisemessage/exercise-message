package com.exercise.message.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import com.exercise.message.entities.Usuario;

@Named
public class UsuarioDAO {

	@PersistenceContext(unitName = "exercise-message")
	private EntityManager entity;
	@Resource
	private UserTransaction userTransaction;

	public void Save(Usuario u) throws Exception {
		userTransaction.begin();
		entity.persist(u);
		userTransaction.commit();

	}

	public void Update(Usuario dato) throws Exception {
		userTransaction.begin();
		entity.merge(dato);
		userTransaction.commit();
	}

	public Usuario BuscarUsuarioById(Integer id) {
		return entity.find(Usuario.class, id);
	}

	@SuppressWarnings("unchecked")
	public Usuario BuscarUsuarioByNombre(String nombre) throws Exception {

		String consulta = "SELECT us FROM Usuario us WHERE  us.nombre=:nombre";
		Query qu = entity.createQuery(consulta).setParameter("nombre", nombre);
		List<Usuario> lista = qu.getResultList();
		return lista.isEmpty() ? null : lista.get(0);
	}
}
